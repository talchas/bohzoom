﻿# Book of Hours Zoom Mod

![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)

Several bits of the zoom behavior are janky or could use other QoL improvements.

In particular this mod
 - changes zoom-to-mouse to zoom towards the mouse without actually moving the cursor, keeping it in the same location both on the screen and in the world
 - changes the keyboard zoom to be continuous again, and gives a little more control to the mouse wheel zoom (hopefully)
 - changes the mid zoom button (2) to zoom in just far enough to click workbenches

 Each of these modules can be enabled/disabled in the config file (which will show up in BepInEx/config/BoHZoom.cfg once you start the game with the mod installed).

 ## Installation

 This is set up to use the [BepInEx5](https://github.com/BepInEx/BepInEx/releases) modloader, so download and install BepInEx 5 (not beta versions of 6). Run the game once to generate directories. Then copy this dll to `<Steam>/Book of Hours/BepInEx/plugins`.

 ## Building

 I built it using VS 2022; other versions of the MS build tools presumably would work. The project has a post-build copy to my steam dir, so you may want to remove that.