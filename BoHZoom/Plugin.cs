﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using SecretHistories.Assets.Scripts.Application.UI.Settings;
using SecretHistories.Constants;
using SecretHistories.Environment;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections;
using SecretHistories.UI;
using SecretHistories.Spheres;
using SecretHistories.Spheres.Choreographers;
using SecretHistories.Infrastructure;

namespace BoHZoom
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        static ManualLogSource Log;
        private ConfigEntry<bool> configMouse;
        private ConfigEntry<bool> configBehavior;
        private ConfigEntry<bool> configMid;

        private void Awake()
        {
            Log = this.Logger;
            configMouse = Config.Bind("Parts", "changeZoomToMouse", true, "Changes Zoom-to-Mouse to keep the mouse over the same world location, instead of centering on it");
            configBehavior = Config.Bind("Parts", "changeZoomBehavior", true, "Changes zoom in/out to not be step-based with keyboard/buttons, and imo a bit better with mousewheel");
            configMid = Config.Bind("Parts", "changeZoomMiddle", true, "Zooms a tiny bit further in with zoom 2, so that workbenches are active");
            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");
            var h = new Harmony(PluginInfo.PLUGIN_GUID);
            if (configMouse.Value)
                h.PatchAll(typeof(PatchZoomToMouse));
            if (configBehavior.Value)
                h.PatchAll(typeof(PatchZoomFluidity));
            if (configMid.Value)
                h.PatchAll(typeof(BetterZoomMid));
            h.PatchAll(typeof(PatchOverlap));
            h.PatchAll(typeof(PatchHandSizes));
            h.PatchAll(typeof(PatchFixSortKeyboard));
            var count = h.GetPatchedMethods().Count();
            Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} patched {count} methods");
        }

        [HarmonyPatch(typeof(CamOperator))]
        static class PatchZoomToMouse
        {
            [HarmonyPatch(nameof(CamOperator.SetCurrentZoomHeight))]
            [HarmonyPrefix]
            static bool SetCurrentZoomHeight(float zoomHeight, Camera ___attachedCamera, ZoomEffectController ___zec, SettingWatcherBool ____zoomToCursorWatcher,
                RectTransform ___navigationLimits, Coroutine ____forcedCameraMotion, ref Vector2 ___smoothTargetPositionXY)
            {

                Vector3 position = ___attachedCamera.transform.position;
                if (____zoomToCursorWatcher.GetValue() && ____forcedCameraMotion == null) {
                    Vector2 screenPoint = Mouse.current.position.ReadValue();
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(___navigationLimits, screenPoint, ___attachedCamera, out var target);

                    // lalala math is hard
                    ___attachedCamera.transform.position = new Vector3(position.x, position.y, zoomHeight);
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(___navigationLimits, screenPoint, ___attachedCamera, out var start);
                    // You might think we'd need to do some scaling or something, but no, you want ~1 camera X/Y for 1 LocalPointInRectangle X/Y
                    // and if you do the obvious scaling things to try to be a linear approximation of whatever camera function it'll be very wrong
                    // Why? I dunno, 3D camera is always try things and see what works for me.

                    Vector2 diff = target - start;
                    ___attachedCamera.transform.position = new Vector3(position.x + diff.x, position.y + diff.y, zoomHeight);
                    ___smoothTargetPositionXY += diff;
#if false
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(___navigationLimits, screenPoint, ___attachedCamera, out var result);

                    Log.LogInfo($"Initial diff: {target - start}, scale: 1 lol, result: {result - target}");
                    Log.LogInfo($" (moved {result - start}; actual motion: {diff})");
#endif
                } else {
                    ___attachedCamera.transform.position = new Vector3(position.x, position.y, zoomHeight);
                }

                ___zec.ApplyZEffects(zoomHeight);
                return false;
            }

            // just disable the function
            [HarmonyPatch("TryAdjustCameraMovementTowardsCursor")]
            [HarmonyPrefix]
            static bool TryAdjustCameraMovementTowardsCursor()
            {
                return false;
            }
        }

        [HarmonyPatch(typeof(CamOperator))]
        class PatchZoomFluidity
        {
            // I _think_ what feels bad about the new vanilla mousewheel zoom with a clicky wheel is that spinning faster does basically no speedup pretty quickly,
            // and if you turn up the speed you lose too much control.
            // Keyboard goes from actually-continuous just 0.3s scroll events repeating, maybe continuous wheel winds up similar;
            // depends on how fast unity gives events for it I guess?

            // This fixes keyboard to actually be continuous, but clicky scroll wheel is still meh tbh
            // A lot of it is that z-distance is very nonlinear in effective zoom behavior and both this and vanilla ignore that.


            static bool ourZoom;
            static float zoomStrength;

            private static IEnumerator DoZoom(CamOperator instance, float easingDuration)
            {
                float lastZoomStrength = 0;
                float easingPassed = 0;
                while (easingPassed < easingDuration) {
                    float delta;
                    if (zoomStrength != 0) {
                        lastZoomStrength = zoomStrength;
                        // the easing starts at 3x, so just weight this accordingly
                        delta = 3 * Time.deltaTime * zoomStrength;
                        easingPassed = 0;
                    } else {
                        // The easing should wind up being an extra easingDuration * zoomStrength total zoom, ie an extra duration/3 worth of holding a continuous zoom
                        float prev = Easing.Cubic.Out(easingPassed / easingDuration);
                        easingPassed += Time.deltaTime;
                        delta = (Easing.Cubic.Out(easingPassed / easingDuration) - prev) * lastZoomStrength * easingDuration;
                    }
                    float newz = instance.GetCurrentZoomHeight() + delta;
                    if (newz < instance.ZOOM_Z_FARTHEST) {
                        instance.SetCurrentZoomHeight(instance.ZOOM_Z_FARTHEST);
                        break;
                    } else if (newz > instance.ZOOM_Z_CLOSE) {
                        instance.SetCurrentZoomHeight(instance.ZOOM_Z_CLOSE);
                        break;
                    } else {
                        instance.SetCurrentZoomHeight(newz);
                    }
                    yield return null;
                }
                ourZoom = false;
            }

            [HarmonyPatch(nameof(CamOperator.OnZoomEvent))]
            [HarmonyPrefix]
            static bool OnZoomEvent(ZoomLevelEventArgs args, float ___zoomStepChangeDuration, ref Coroutine ___zoomCoroutine, SettingWatcherInt ____zoomStepDistanceWatcher, CamOperator __instance)
            {
                if (args.AbsoluteTargetZoomLevel != ZoomLevel.Unspecified)
                    return true;

                // interrupt other zooms
                if (___zoomCoroutine != null) {
                    __instance.StopCoroutine(___zoomCoroutine);
                }

                if (Math.Abs(args.CurrentZoomInput) > 0.01) {
                    float factor = 3;
                    if (args.Repeat) // keyboard
                        factor = 1;
                    zoomStrength = -args.CurrentZoomInput * ____zoomStepDistanceWatcher.GetValue() * factor;
                    if (!ourZoom) {
                        ourZoom = true;
                        __instance.StartCoroutine(DoZoom(__instance, ___zoomStepChangeDuration / factor));
                    }
                } else {
                    zoomStrength = 0;
                }

                return false;
            }
        }

        [HarmonyPatch(typeof(CamOperator))]
        static class BetterZoomMid
        {
            [HarmonyPatch(nameof(CamOperator.OnZoomEvent))]
            [HarmonyPostfix]
            static void OnZoomEventPostfix(ZoomLevelEventArgs args, CamOperator __instance, float ___zoomLevelChangeDuration)
            {
                // Override mid zoom to zoom in a tiny bit further so that workstations are enabled
                if (args.AbsoluteTargetZoomLevel == ZoomLevel.Mid)
                    __instance.RequestZoom(__instance.ZOOM_Z_MID + 200, ___zoomLevelChangeDuration);
            }
        }

        [HarmonyPatch(typeof(Meniscate))]
        static class PatchOverlap
        {
            static string currentSphere;
            [HarmonyPatch(nameof(Meniscate.GetAutoSortHorizontalOverlapCoefficientForHand))]
            [HarmonyPrefix]
            static void PrefixCoeff(string sphereId)
            {
                currentSphere = sphereId;
            }
            [HarmonyPatch(nameof(Meniscate.GetEarlierCardsOnTop))]
            [HarmonyPrefix]
            static bool ReplaceEarlierCards(ref bool __result)
            {
                if (currentSphere == "hand.skills") {
                    __result = false;
                    return false;
                }
                return true;
            }
        }
        [HarmonyPatch(typeof(UIHandSphere))]
        static class PatchHandSizes
        {
            [HarmonyPatch("Start")]
            [HarmonyPostfix]
            static void Debug(UIHandSphere __instance, UIWidgets.DraggableUI ___draggable)
            {
                //___draggable.Horizontal = true;
                Log.LogInfo($"{__instance.Id}: {___draggable.RectTransform.anchoredPosition} {___draggable.RectTransform.rect}");
            }
        }
        static class PatchFixSortKeyboard
        {
            [HarmonyPatch(typeof(UIController),nameof(UIController.Input_GroupAllStacks))]
            [HarmonyPostfix]
            static void Debug(HandChoreographer __instance)
            {
                foreach (var item in GameObject.Find("UIHand").GetComponentsInChildren<HandChoreographer>()) {
                    item.SortCards();
                }
            }
        }
    }
}
